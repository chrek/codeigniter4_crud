# CodeIgniter 4 CRUD Application

This basic CRUD application uses CodeIgniter4 framework and the basic principles of MVC architecture.

## Features
- Ability to perform CRUD (Create Read Update Delete) operations using database in MySQL server
- The public and index segments are not part of the URLs used:
    - http://localhost:8080/apiusers
    - http://localhost:8080/apiusers/create
    - http://localhost:8080/apiusers/edit/2
    - http://localhost:8080/apiusers/delete/1

## Database
- The table used is named apiusers with 2 allowedFields / required fields (name and email)
- The database details are defined in a .env file as:
  - database.default.hostname = localhost
  - database.default.database = website_db
  - database.default.username = root
  - database.default.password = ""
  - database.default.DBDriver = MySQLi

## Tools used
- Bootstrap 4.5.1
- jQuery DataTables plug-in
- jQuery Validation plug-in

## References
- [tutsmake.com](https://www.tutsmake.com/codeigniter-4-first-crud-example-tutorial/)
- [tutorialrepublic.com](https://www.tutorialrepublic.com/codelab.php?topic=bootstrap&file=crud-data-table-for-database-with-modal-form)
- [datatables.net](https://datatables.net/manual/installation)


## ----------- About CodeIgniter -----------------
## What is CodeIgniter?

CodeIgniter is a PHP full-stack web framework that is light, fast, flexible, and secure. 
More information can be found at the [official site](http://codeigniter.com).

The user guide corresponding to this version of the framework can be found
[here](https://codeigniter4.github.io/userguide/). 

## Installation & updates

`composer create-project codeigniter4/appstarter` then `composer update` whenever
there is a new release of the framework.

When updating, check the release notes to see if there are any changes you might need to apply
to your `app` folder. The affected files can be copied or merged from
`vendor/codeigniter4/framework/app`.


## Important Change with index.php

`index.php` is no longer in the root of the project! It has been moved inside the *public* folder,
for better security and separation of components.

This means that you should configure your web server to "point" to your project's *public* folder, and
not to the project root. A better practice would be to configure a virtual host to point there. A poor practice would be to point your web server to the project root and expect to enter *public/...*, as the rest of your logic and the
framework are exposed.

**Please** read the user guide for a better explanation of how CI4 works!
The user guide updating and deployment is a bit awkward at the moment, but we are working on it!

## Server Requirements

PHP version 7.2 or higher is required, with the following extensions installed: 

- [intl](http://php.net/manual/en/intl.requirements.php)
- [libcurl](http://php.net/manual/en/curl.requirements.php) if you plan to use the HTTP\CURLRequest library

Additionally, make sure that the following extensions are enabled in your PHP:

- json (enabled by default - don't turn it off)
- [mbstring](http://php.net/manual/en/mbstring.installation.php)
- [mysqlnd](http://php.net/manual/en/mysqlnd.install.php)
- xml (enabled by default - don't turn it off)