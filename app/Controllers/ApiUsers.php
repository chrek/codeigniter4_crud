<?php

namespace App\Controllers;

use App\Models\ApiUsersModel;
use CodeIgniter\Controller;

class ApiUsers extends Controller
{
  public function index()
  {
    $model = new ApiUsersModel();
    $data['users'] = $model->orderBy('id', 'DESC')->findAll();
    return view('users', $data);
  }

  public function create()
  {
    return view('create-user');
  }

  public function store()
  {
    helper(['form', 'url']);
    $model = new ApiUsersModel();

    $data = [
      'name' => $this->request->getVar('name'),
      'email' => $this->request->getVar('email'),
    ];

    $save = $model->insert($data);

    //return redirect()->to(base_url('public/index.php/users'));
    return redirect()->to(base_url('apiusers'));
  }

  public function edit($id = null)
  {
    $model = new ApiUsersModel();
    $data['user'] = $model->where('id', $id)->first();
    //return view('public/index.php/edit-user', $data);
    return view('edit-user', $data);
  }

  public function update()
  {
    helper(['form', 'url']);
    $model = new ApiUsersModel();
    $id = $this->request->getVar('id');
    $data = [
      'name' => $this->request->getVar('name'),
      'email' => $this->request->getVar('email'),
    ];

    $save = $model->update($id, $data);
    //return redirect()->to(base_url('public/index.php/users'));
    return redirect()->to(base_url('apiusers'));
  }

  public function delete($id = null)
  {
    $model = new ApiUsersModel();
    $data['user'] = $model->where('id', $id)->delete();
    //return redirect()->to(base_url('public/index.php/users'));
    return redirect()->to(base_url('apiusers'));
  }
}
