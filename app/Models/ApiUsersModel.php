<?php

namespace App\Models;

use CodeIgniter\Model;

class ApiUsersModel extends Model
{
  protected $table = 'apiusers';
  protected $allowedFields = ['name', 'email'];
}
